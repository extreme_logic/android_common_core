package ph.extremelogic.android.common.core;

import java.io.File;
import java.io.FileFilter;
import java.util.regex.Pattern;

import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Build.VERSION;
import android.os.Environment;
import android.os.StatFs;
import android.view.WindowManager;

/**
 * Information class for your Android device.
 */
public class Info {

    /**
     * Retrieves the android version name.
     * @return Android name
     */
    public static String getAndroidVersionName() {
        int apiLevel = VERSION.SDK_INT;
        String versionName = "Unknown";
        switch (apiLevel) {
            case 1: versionName = "No code name"; break;
            case 2: versionName = "No code name"; break;
            case 3: versionName = "Cupcake"; break;
            case 4: versionName = "Donut"; break;
            case 5: versionName = "Eclair"; break;
            case 6: versionName = "Eclair"; break;
            case 7: versionName = "Eclair"; break;
            case 8: versionName = "Froyo"; break;
            case 9: versionName = "Gingerbread"; break;
            case 10: versionName = "Gingerbread"; break;
            case 11: versionName = "Honeycomb"; break;
            case 12: versionName = "Honeycomb"; break;
            case 13: versionName = "Honeycomb"; break;
            case 14: versionName = "Ice Cream Sandwich"; break;
            case 15: versionName = "Ice Cream Sandwich"; break;
            case 16: versionName = "Jelly Bean"; break;
            case 17: versionName = "Jelly Bean"; break;
            case 18: versionName = "Jelly Bean"; break;
            case 19: versionName = "Kitkat"; break;
            case 20: versionName = "Kitkat"; break;
            case 21: versionName = "Lollipop"; break;
            case 22: versionName = "Lollipop"; break;
            case 23: versionName = "Marshmallow"; break;
            case 24: versionName = "Nougat"; break;
            case 25: versionName = "Nougat"; break;
            case 26: versionName = "Oreo"; break;
            case 27: versionName = "Oreo"; break;
            case 28: versionName = "Pie"; break;
            case 29: versionName = "Q"; break;
        }
        return versionName;
    }

    /**
     * Interprets core for humans.
     *
     * @param coreCount Core count parameter.
     * @return String  representing the core count.
     */
    public static String interpretCore(int coreCount) {
        switch (coreCount) {
            case 4:
                return "Quad";
            case 1:
                return "Single";
            case 2:
                return "Dual";
            case 8:
                return "Octa";
            default:
                return "" + coreCount;
        }
    }

    /**
     * Retrieves the number of cores of your device.
     *
     * @return Number of cores.
     */
    public static int getNumCores() {
        try {
            int var2 = (new File("/sys/devices/system/cpu/"))
                    .listFiles(new CpuFilter()).length;
            return var2;
        } catch (Exception var3) {
            return 1;
        }
    }

    /**
     * Determines if device display is in landscape.
     *
     * @param ctx Context.
     * @return true if display is in landscape.
     */
    public static boolean isLandscapeDevice(Context ctx) {
        if (VERSION.SDK_INT >= 8) {
            int var1 = ((WindowManager) ctx.getSystemService("window"))
                    .getDefaultDisplay().getRotation();
            return ctx.getResources().getConfiguration().orientation == 2 ? var1 == 0
                    || var1 == 2
                    : var1 == 1 || var1 == 3;
        } else {
            return false;
        }
    }

    /**
     * Determines if a memory card is present.
     *
     * @return true of a card is present.
     */
    @SuppressLint("NewApi")
	public static boolean isSDCardPresent() {
    	boolean isSDCardPresent = false;
    	
    	if(Environment.getExternalStorageState()
        .equals(Environment.MEDIA_MOUNTED ) && Environment.isExternalStorageRemovable()) {
    		isSDCardPresent = true;
    	}

    	return isSDCardPresent;
    }

    /**
     * Retrieves information about the memory.
     *
     * @return Memory object with various information.
     */
    @SuppressLint("NewApi")
	public static Memory getSDCardInfo(){
    	Memory mem = new Memory();
    	StatFs statFs = new StatFs(Environment.getExternalStorageDirectory().getAbsolutePath());        
    	 long blockSize = statFs.getAvailableBlocksLong();
    	 long totalSize = statFs.getBlockCountLong()*blockSize;
    	 long availableSize = statFs.getAvailableBlocksLong()*blockSize;
    	 long freeSize = statFs.getFreeBlocksLong()*blockSize;
    	 
    	 mem.setBlockSize(blockSize);
    	 mem.setTotalSize(totalSize);
    	 mem.setAvailableSize(availableSize);
    	 mem.setFreeSize(freeSize);
    	 
    	return mem;
    }
    
    static class CpuFilter implements FileFilter {
        public boolean accept(File var1) {
            return Pattern.matches("cpu[0-9]", var1.getName());
        }
    }
}
