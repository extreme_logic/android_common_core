package ph.extremelogic.android.common.core.activity;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

import ph.extremelogic.android.common.core.frag.SettingFragment;

/**
 * Created by vs186018 on 6/17/2016.
 */
public class SettingActivity extends PreferenceActivity implements
        SharedPreferences.OnSharedPreferenceChangeListener {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(android.R.id.content, new SettingFragment()).commit();
        //getPreferenceScreen().findPreference("prefSMSSend").setEnabled(false);
        //Preference.getPreferenceManager().findPreference("prefSMSSend").setEnabled(false);
    }


    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {
    }
}