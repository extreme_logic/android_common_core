package ph.extremelogic.android.common.core.frag;


import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import ph.extremelogic.android.common.core.R;

@SuppressLint("NewApi")
public class AboutDialogFragment  extends DialogFragment {

    /*
     * (non-Javadoc)
     * @see android.app.DialogFragment#onCreateDialog(android.os.Bundle)
     */
    @SuppressLint("InflateParams")
	public Dialog onCreateDialog(Bundle savedInstanceState) {
        Activity activity = null;
        AlertDialog.Builder builder = null;
        LayoutInflater inflater = null;
        View about = null;
        TextView version = null;

        activity = getActivity();
        builder = new AlertDialog.Builder(activity);
        inflater = activity.getLayoutInflater();

        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        about = inflater.inflate(R.layout.layout_about, null);

        version = (TextView) about.findViewById(R.id.app_name_version);
        version.setText(getString(R.string.app_name, "") + " " + getVersion());

        builder.setView(about)
        // Add action buttons
                .setPositiveButton(R.string.positive_ok,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int id) {
                            }
                        });

        return builder.create();
    }

    /**
     * @return version
     */
    private String getVersion() {
        PackageInfo pinfo = null;
        Context ctx = null;
        Activity activity = null;
        PackageManager pm = null;
        String versionName = "";

        activity = getActivity();
        ctx = activity.getApplicationContext();
        pm = activity.getPackageManager();
        try {
            pinfo = pm.getPackageInfo(ctx.getPackageName(), 0);
            versionName = pinfo.versionName;
        } catch (NameNotFoundException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
        return versionName;
    }
}
