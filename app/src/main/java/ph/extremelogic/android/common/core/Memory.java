package ph.extremelogic.android.common.core;

public class Memory {
	private long blockSize;
	private long totalSize;
	private long availableSize;
	private long freeSize;

	public long getBlockSize() {
		return blockSize;
	}

	public void setBlockSize(long blockSize) {
		this.blockSize = blockSize;
	}

	public long getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(long totalSize) {
		this.totalSize = totalSize;
	}

	public long getAvailableSize() {
		return availableSize;
	}

	public void setAvailableSize(long availableSize) {
		this.availableSize = availableSize;
	}

	public long getFreeSize() {
		return freeSize;
	}

	public void setFreeSize(long freeSize) {
		this.freeSize = freeSize;
	}
}
