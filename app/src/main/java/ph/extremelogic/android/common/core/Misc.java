package ph.extremelogic.android.common.core;

import android.app.ActivityManager;
import android.app.ActivityManager.RunningTaskInfo;
import android.content.ActivityNotFoundException;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.provider.ContactsContract;
import android.provider.ContactsContract.CommonDataKinds;
import android.provider.ContactsContract.CommonDataKinds.Phone;
import android.telephony.SmsManager;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Jan So
 */
public class Misc {
    /**
     * @param id  Contact identifier.
     * @param ctx Context of the application.
     * @return List of mobile numbers identified using the id.
     */
    public static ArrayList<String> getNumberByID(final String id,
                                                  final Context ctx) {
        ArrayList<String> phones = new ArrayList<String>();
        ContentResolver bd = null;

        bd = ctx.getContentResolver();
        Cursor cursor = bd.query(Phone.CONTENT_URI, null,
                Phone.CONTACT_ID + " = ?", new String[]{id},
                null);

        while (cursor.moveToNext()) {
            String number = cursor.getString(cursor
                    .getColumnIndex(Phone.NUMBER));
            int type = cursor.getInt(cursor.getColumnIndex(Phone.TYPE));
            switch (type) {
                case Phone.TYPE_HOME:
                    // do something with the Home number here...
                    break;
                case Phone.TYPE_MOBILE:
                    phones.add(number);
                    break;
                case Phone.TYPE_WORK:
                    // do something with the Work number here...
                    break;
            }
        }

        cursor.close();
        if (phones.isEmpty()) {
            return null;
        } else {
            return phones;
        }
    }

    /**
     * @param ctx Context of the application.
     * @return true if the app is the current app being used else false.
     */
    public static boolean isAppActive(final Context ctx) {
        boolean isAppActive = false;
        ActivityManager activityManager = (ActivityManager) ctx
                .getSystemService(Context.ACTIVITY_SERVICE);
        List<RunningTaskInfo> services = activityManager
                .getRunningTasks(Integer.MAX_VALUE);
        boolean isActivityFound = false;

        if (services.get(0).topActivity.getPackageName().toString()
                .equalsIgnoreCase(ctx.getPackageName().toString())) {
            isActivityFound = true;
        }

        if (isActivityFound) {
            isAppActive = true;
        } else {
            // write your code to build a notification.
            // return the notification you built here
        }
        return isAppActive;
    }

    /**
     * @param mobileNo Target mobile number for the SMS.
     * @param message  Message content of the SMS.
     */
    public static void sendSMS(final String mobileNo, final String message) {
        SmsManager smsManager = SmsManager.getDefault();
        smsManager.sendTextMessage(mobileNo, null, message, null, null);
    }

    /**
     * @param context Context of the application.
     */
    public static void visitMarket(final Context context) {
        Uri uri = Uri.parse("market://details?id=" + context.getPackageName());
        Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
        // To count with Play market backstack, After pressing back button,
        // to taken back to our application, we need to add following flags to
        // intent.
        goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY
                | Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET
                | Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
        try {
            context.startActivity(goToMarket);
        } catch (ActivityNotFoundException e) {
            context.startActivity(new Intent(Intent.ACTION_VIEW, Uri
                    .parse("http://play.google.com/store/apps/details?id="
                            + context.getPackageName())));
        }
    }

    public static void toast(String message, Context context) {
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context,
                message, duration);
        toast.show();
    }

    /**
     * @param context Context of the application.
     * @param text    Text to be copied to the clipboard
     */
    public static void copyToClipboard(Context context, final String text) {
        int sdk = android.os.Build.VERSION.SDK_INT;
        if (sdk < android.os.Build.VERSION_CODES.HONEYCOMB) {
            android.text.ClipboardManager clipboard = null;
            clipboard = (android.text.ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);
            clipboard.setText(text);
        } else {
            android.content.ClipboardManager clipboard = null;
            clipboard = (android.content.ClipboardManager) context.getSystemService(context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = null;
            clip = android.content.ClipData
                    .newPlainText("text clipboard", text);
            clipboard.setPrimaryClip(clip);
        }
    }

    /**
     * @param context Context of the application
     * @param number  Mobile number
     * @return true if the number is in the address book
     */
    public boolean contactExists(Context context, String number) {
        Uri lookupUri = Uri.withAppendedPath(
                ContactsContract.PhoneLookup.CONTENT_FILTER_URI,
                Uri.encode(number));
        String[] mPhoneNumberProjection = {ContactsContract.PhoneLookup._ID, ContactsContract.PhoneLookup.NUMBER, ContactsContract.PhoneLookup.DISPLAY_NAME};
        Cursor cur = context.getContentResolver().query(lookupUri, mPhoneNumberProjection, null, null, null);
        try {
            if (cur.moveToFirst()) {
                return true;
            }
        } finally {
            if (cur != null)
                cur.close();
        }
        return false;
    }
}
