package ph.extremelogic.android.common.core;

import java.io.File;
import java.io.FileOutputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.net.Uri;
import android.os.Environment;
import android.support.v4.content.FileProvider;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.View.MeasureSpec;
import static android.view.View.MeasureSpec.makeMeasureSpec;

/**
 * @author Jan So
 */
public final class Droid {
    /**
     * Default constructor.
     */
    private Droid() {
        // do nothing
    }

    /**
     * Default image quality.
     */
    private static final int IMAGE_QUALITY = 100;

    /**
     * Default home path of application.
     */
    private static final String PATH_HOME = "Extreme Logic";

    /**
     * Saves view to bitmap.
     *
     * @param ctx Context
     * @param v Root View
     * @return Bitmap object
     */
    public static Bitmap viewToBitmap(Context ctx, View v) {
        DisplayMetrics dm = null;
        Bitmap bitmap = null;

        dm = ctx.getResources().getDisplayMetrics();
        v.measure(makeMeasureSpec(dm.widthPixels, MeasureSpec.EXACTLY),
                makeMeasureSpec(dm.heightPixels, MeasureSpec.EXACTLY));
        v.layout(0, 0, v.getMeasuredWidth(), v.getMeasuredHeight());
        bitmap = Bitmap.createBitmap(v.getMeasuredWidth(),
                v.getMeasuredHeight(), Bitmap.Config.ARGB_8888);
        Canvas c = new Canvas(bitmap);
        v.draw(c);

        return bitmap;
    }

    public static Uri viewToBitmapUri(String imageFile, View view, Context ctx, String dirToCreate) {
        Image.saveBitMapToDisk(viewToBitmap(ctx, view), imageFile, Environment.DIRECTORY_PICTURES, 100, Bitmap.CompressFormat.PNG, dirToCreate);
        return getFileUri(imageFile, dirToCreate);
    }

    public static Uri viewToBitmapUri(String imageFile, View view, Context ctx, String packageProvider, String path, String dirToCreate) {
        Image.saveBitMapToDisk(viewToBitmap(ctx, view), imageFile, path, 100, Bitmap.CompressFormat.PNG, dirToCreate);
        return getExternalFileUri(path, imageFile, ctx, packageProvider);
    }

    public static Uri getFileUri(String filename, String dirToCreate) {
        if (filename != null) {
            StringBuilder sb;
            sb = new StringBuilder();
            sb.append(Environment.getExternalStorageDirectory());
            sb.append(File.separator);
            sb.append(Environment.DIRECTORY_PICTURES);
            if (dirToCreate != null) {
                sb.append(File.separator);
                sb.append(dirToCreate);
            }
            return Uri.fromFile(new File(sb.toString(), filename));
        }
        throw new AssertionError();
    }


    public static Uri getExternalFileUri(String path, String filename, Context ctx, String packageProvider) {
        if (filename != null) {
            StringBuilder sb;
            sb = new StringBuilder();
            sb.append(Environment.getExternalStorageDirectory());
            sb.append(File.separator);
            sb.append(path);
            return FileProvider.getUriForFile(ctx, ctx.getApplicationContext().getPackageName() + "." + packageProvider, new File(sb.toString(), filename));
        }
        throw new AssertionError();
    }

    public static Uri viewToBitmapUri(final String imageFile, View view,
                                      Context ctx) {
        Bitmap img;
        img = viewToBitmap(ctx, view);
        saveToDisk(img, imageFile);
        return getFileUri(imageFile);
    }

    /**
     * @param bitmap Bitmap
     * @param fileName File name
     */
    public static String saveToDisk(Bitmap bitmap, String fileName) {
        StringBuilder path = null;
        File sd = null;
        File dest = null;
        FileOutputStream out = null;

        path = new StringBuilder();
        path.append(Environment.DIRECTORY_PICTURES);
        path.append(File.separator);
        // path.append(PATH_HOME);
        // path.append(File.separator);
        path.append(fileName);

        sd = Environment.getExternalStorageDirectory();
        dest = new File(sd, path.toString());

        try {
            out = new FileOutputStream(dest);
            bitmap.compress(Bitmap.CompressFormat.PNG, IMAGE_QUALITY, out);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            sd = null;
            dest = null;
            out = null;
        }
        return path.toString();
    }

    /**
     * @param filename File name.
     * @return URI of the file.
     */
    public static Uri getFileUri(String filename) {
        assert filename != null;

        StringBuilder sb;
        File file = null;
        sb = new StringBuilder();
        sb.append(Environment.getExternalStorageDirectory());
        sb.append(File.separator);
        sb.append(Environment.DIRECTORY_PICTURES);
        // sb.append(File.separator);
        // sb.append(PATH_HOME);

        file = new File(sb.toString(), filename);
        return Uri.fromFile(file);
    }
}

