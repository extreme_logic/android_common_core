package ph.extremelogic.android.common.core.frag;

import android.os.Bundle;
import android.preference.PreferenceFragment;

import ph.extremelogic.android.common.core.R;

/**
 * Created by vs186018 on 6/17/2016.
 */
public class SettingFragment extends PreferenceFragment {
    @Override
    public final void onCreate(final Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        addPreferencesFromResource(R.xml.settings);
    }
}
