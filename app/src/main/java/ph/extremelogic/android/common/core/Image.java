package ph.extremelogic.android.common.core;

import java.io.File;
import java.io.FileOutputStream;

import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.PorterDuff.Mode;
import android.graphics.PorterDuffXfermode;
import android.graphics.Rect;
import android.os.Environment;

/**
 * @author Jan So
 */
public class Image {

	/**
	 * @param bitmap
	 *            - Bitmap image.
	 * @return Cropped bitmap.
	 */
	public static Bitmap getCroppedBitmap(Bitmap bitmap) {
		Bitmap output = Bitmap.createBitmap(bitmap.getWidth(),
				bitmap.getHeight(), Config.ARGB_8888);
		Canvas canvas = new Canvas(output);

		final int color = 0xff424242;
		final Paint paint = new Paint();
		final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());

		paint.setAntiAlias(true);
		canvas.drawARGB(0, 0, 0, 0);
		paint.setColor(color);
		// canvas.drawRoundRect(rectF, roundPx, roundPx, paint);
		canvas.drawCircle(bitmap.getWidth() / 2, bitmap.getHeight() / 2,
				bitmap.getWidth() / 2, paint);
		paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
		canvas.drawBitmap(bitmap, rect, rect, paint);
		// Bitmap _bmp = Bitmap.createScaledBitmap(output, 60, 60, false);
		// return _bmp;
		return output;
	}

	/**
	 * @param bitmap
	 *            Bitmap
	 * @param fileName
	 *            File name
	 */
	public static String saveBitMapToDisk(Bitmap bitmap, String fileName,
			String dirEnv, final int imageQuality,
			final CompressFormat compressfmt, final String dirToCreate) {
		StringBuilder path = null;
		File sd = null;
		File dest = null;
		FileOutputStream out = null;

		// create the picture directory if not existing
		File directory = new File(Environment.getExternalStorageDirectory()
				+ File.separator + dirEnv + File.separator + dirToCreate);

		//File directory = new File(Environment.getExternalStoragePublicDirectory(dirEnv), appName);

		if (!directory.exists()) {
			directory.mkdirs();
		}

		path = new StringBuilder();
		path.append(dirEnv);
		path.append(File.separator);
		path.append(dirToCreate);
		path.append(File.separator);
		path.append(fileName);

		sd = Environment.getExternalStorageDirectory();
		//sd = Environment.getExternalStoragePublicDirectory(dirEnv);
		dest = new File(sd, path.toString());

		try {
			out = new FileOutputStream(dest);
			bitmap.compress(compressfmt, imageQuality, out);
			out.flush();
			out.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			sd = null;
			dest = null;
			out = null;
		}
		return path.toString();
	}
}
